"use strict";
(function ($) {
    // Start the carousel, this can't be run async (in render)
    const owl = $("#customers-testimonials").owlCarousel({
        nav: true,
        rtl: true,
        items: 1,
        autoplay: true,
        loop: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        navText: [
            "<i class='fas fa-chevron-left owl-nav-left'></i>",
            "<i class='fas fa-chevron-right owl-nav-right'></i>",
        ],
    });
    // Fetch the students
    fetch('assets/data/estudiantes.json')
        .then(res => res.json())
        .then(json => json.data)
        .then(data => render(data));
    // Inserts the carousel items
    function render(estudiantes) {
        // Shuffle the students
        shuffle(estudiantes);
        // Adds the items
        for (const e of estudiantes) {
            owl.trigger('add.owl.carousel', createItem(e));
        }
        // Notifies the carousel
        owl.trigger('refresh.owl.carousel');
    }
    function createItem(estudiante) {
        const opinion = estudiante.opinion
            .split("\n")
            .map(e => `<p class="direction-ltr">${e}</p>`)
            .join("\n");
        return `
        <div class="item">
            <img src="assets/images/estudiantes/${estudiante.foto}" class="clients m-auto" alt="${estudiante.nombre}" />
            <i class="fas fa-quote-left quote my-4"></i>
                <div class="px-5">
                    ${opinion}
                </div>
            <h6 class="name mt-4">${estudiante.nombre}</h6>
            <span class="font-weight-bold">${estudiante.ocupacion}</span>
        </div>`;
    }
    function shuffle(array) {
        for (let i = 0; i < array.length; i++) {
            const newIndex = randomRange(i, array.length);
            const temp = array[i];
            array[i] = array[newIndex];
            array[newIndex] = temp;
        }
    }
    function randomRange(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
})(jQuery);
